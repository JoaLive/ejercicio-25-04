﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Ciudad: ");
                string city = Console.ReadLine();

                WebRequest wReq = WebRequest.Create("https://maps.googleapis.com/maps/api/geocode/json?address=" + city);
                WebResponse wRta = wReq.GetResponse();

                Stream stream = wRta.GetResponseStream();
                StreamReader strReader = new StreamReader(stream);

                JObject jObj = JObject.Parse(strReader.ReadToEnd());
                JToken jTok = jObj["results"][0]["address_components"][3]["long_name"];
                JsonReader jReader = jTok.CreateReader();

                Console.WriteLine(jReader.ReadAsString());         
                Console.ReadKey();

                stream.Close();
                strReader.Close();
            }
            catch (System.Net.WebException)
            {
                Console.WriteLine("No hay conexion a internet");
                Console.ReadKey();
            }
            catch(ArgumentOutOfRangeException)
            {
                Console.WriteLine("No es una ciudad valida");
                Console.ReadKey();
            }
        }
    }
}
