﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;

namespace Ejercicio25_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Movie: ");
            string movie = Console.ReadLine();

            try
            {
                WebRequest wReq = WebRequest.Create("http://www.omdbapi.com/?t=" + movie);
                WebResponse wRta = wReq.GetResponse();

                Stream stream = wRta.GetResponseStream();
                StreamReader strReader = new StreamReader(stream);

                JObject jObj = JObject.Parse(strReader.ReadToEnd());    

                Console.WriteLine(jObj["Year"]);
                Console.ReadKey();

                strReader.Close();
                stream.Close();
            }
            catch (System.Net.WebException)
            {
                Console.WriteLine("Sin conexion a internet");
                Console.ReadKey();
            }
        }
    }
}
